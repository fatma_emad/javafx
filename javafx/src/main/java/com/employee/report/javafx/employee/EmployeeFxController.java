package com.employee.report.javafx.employee;

import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.dms.constant.Department;
import com.example.dms.criteria.EmployeeCriteria;
import com.example.dms.parameters.EmployeeSearchParameters;
import com.example.dms.response.EmployeeResponse;
import com.example.dms.rest.EmployeeRestContoller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import jfxtras.scene.control.CalendarTextField;

@Component
public class EmployeeFxController {
	@FXML
	private TableColumn<EmployeeFxBean, String> ContractTypeCol;
	@FXML
	private TableColumn<EmployeeFxBean, String> employeeNameCol;

	@FXML
	private TableView<EmployeeFxBean> empolyeeTableView;
	@FXML
	private TableColumn<EmployeeFxBean, String> birthCityCol;

	@FXML
	private TableColumn<EmployeeFxBean, LocalDate> birthDateCol;

	@FXML
	private TableColumn<EmployeeFxBean, Department> departmentCol;
	@FXML
	private TableColumn<EmployeeFxBean, String> directManagerCol;
	@FXML
	private TableColumn<EmployeeFxBean, String> employeeCodeCol;
	@FXML
	private TableColumn<EmployeeFxBean, Long> employeeIdCol;
	@FXML
	private TableColumn<EmployeeFxBean, String> jobTitleCol;

	@FXML
	private TableColumn<EmployeeFxBean, Boolean> statusCol;

	@FXML
	private ComboBox<String> birthCityCOmbo;
	@FXML
	private ComboBox<String> contractTypeCombo;

	@FXML
	private ComboBox<Department> departmentCombo;

	@FXML
	private ComboBox<Boolean> statusCombo;

	@FXML
	private CalendarTextField birthdate;
	@FXML
	private TextField directManger;

	@FXML
	private TextField employeeCode;

	@FXML
	private TextField employeeId;

	@FXML
	private TextField employeeName;

	@FXML
	private TextField jobTitle;
	@Autowired
	private EmployeeRestContoller employeeRestContoller;
	private ObservableList<EmployeeFxBean> employeeTableList;

	@FXML
	public void initialize() {
		initDepartmentComboBox();
		initEmployeeTable();
	}

	private void initEmployeeTable() {
		employeeTableList = FXCollections.observableArrayList();
		ContractTypeCol.setCellValueFactory(p -> p.getValue().contarctTypeProperty());
		employeeNameCol.setCellValueFactory(p -> p.getValue().employeeNameProperty());
		birthCityCol.setCellValueFactory(p -> p.getValue().birthCityProperty());
		birthDateCol.setCellValueFactory(p -> p.getValue().birthDateProperty());
		departmentCol.setCellValueFactory(p -> p.getValue().departmentProperty());
		directManagerCol.setCellValueFactory(p -> p.getValue().directManagerNameProperty());
		employeeCodeCol.setCellValueFactory(p -> p.getValue().employeeCodeProperty());
		employeeIdCol.setCellValueFactory(p -> p.getValue().employeeIdProperty());
		jobTitleCol.setCellValueFactory(p -> p.getValue().jobTitleProperty());
		statusCol.setCellValueFactory(p -> p.getValue().statusProperty());
		empolyeeTableView.setItems(employeeTableList);
	}

	@FXML
	private void search() {
		List<EmployeeResponse> responses = employeeRestContoller
				.search(new EmployeeCriteria(getEmployeeSearchParamters()));
		fillBeanWithResponses(responses);
	}

	private void fillBeanWithResponses(List<EmployeeResponse> responses) {
		for (EmployeeResponse employeeResponse : responses) {
			EmployeeFxBean bean = new EmployeeFxBean();
			bean.birthCityProperty().setValue(employeeResponse.getBirthCity());
			bean.birthDateProperty().setValue(Instant.ofEpochMilli(employeeResponse.getBirthDate().getTime())
					.atZone(ZoneId.systemDefault()).toLocalDate());

			bean.jobTitleProperty().setValue(employeeResponse.getJobTitle());
			bean.contarctTypeProperty().setValue(employeeResponse.getContractType());

			bean.departmentProperty().setValue(employeeResponse.getDepartment());
			bean.directManagerNameProperty().setValue(employeeResponse.getDirectManager().getEmployeeName());

			bean.employeeCodeProperty().setValue(employeeResponse.getEmployeeCode());
			bean.employeeIdProperty().setValue(employeeResponse.getId());

			bean.employeeNameProperty().setValue(employeeResponse.getEmployeeName());
			bean.statusProperty().setValue(employeeResponse.getStatus());
		}

	}

	private EmployeeSearchParameters getEmployeeSearchParamters() {
		EmployeeSearchParameters employeeSearchParameters = new EmployeeSearchParameters();
		employeeSearchParameters.setBirthCity(birthCityCOmbo.getValue());
		employeeSearchParameters.setBirthDate(birthdate.getCalendar().getTime());
		employeeSearchParameters.setContractType(contractTypeCombo.getValue());
		employeeSearchParameters.setDepartment(departmentCombo.getValue());
		employeeSearchParameters.setDirectManagerId(Long.valueOf(directManger.getText()));
		employeeSearchParameters.setEmployeeCode(employeeCode.getText());
		employeeSearchParameters.setJobTitle(jobTitle.getText());
		employeeSearchParameters.setId(Long.valueOf(employeeId.getText()));
		employeeSearchParameters.setStatus(statusCombo.getValue());
		employeeSearchParameters.setEmployeeName(employeeName.getText());
		return employeeSearchParameters;
	}

	private void initDepartmentComboBox() {
		ObservableList<Department> compoBoxBody = FXCollections.observableArrayList();
		compoBoxBody.addAll(Department.values());
		compoBoxBody.add(0, null);
		departmentCombo.setItems(compoBoxBody);

	}

}
