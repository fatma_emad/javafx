package com.employee.report.javafx;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javafx.application.Application;

@SpringBootApplication(
scanBasePackages={"com.example.dms.rest","com.example.dms.service","com.example.dms.repo",
		"com.example.dms.mapper"})
public class JavafxApplication {

	public static void main(String[] args) {
		
			Application.launch(EmployeeReportFxApplication.class, args);

		}

	

}
