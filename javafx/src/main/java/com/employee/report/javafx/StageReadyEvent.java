package com.employee.report.javafx;
import org.springframework.context.ApplicationEvent;

import javafx.stage.Stage;


public class StageReadyEvent extends ApplicationEvent {

	public StageReadyEvent(Stage source) {
		super(source);
	}

	public Stage getStage() {
		return ((Stage)getSource());
	}

}
