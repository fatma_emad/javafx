package com.employee.report.javafx;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;


import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

public class EmployeeReportFxApplication extends Application {
	ConfigurableApplicationContext applicationContext;

	@Override
	public void init() {
		applicationContext = new SpringApplicationBuilder(JavafxApplication.class).run();
	}

	@Override
	public void start(Stage stage) {
		applicationContext.publishEvent(new StageReadyEvent(stage));

	}

	@Override
	public void stop() {
		applicationContext.close();
		Platform.exit();
	}

}
