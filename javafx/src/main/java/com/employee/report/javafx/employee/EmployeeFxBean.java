package com.employee.report.javafx.employee;

import java.time.LocalDate;

import org.springframework.stereotype.Component;

import com.example.dms.constant.Department;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@Component
public class EmployeeFxBean {
	private ObjectProperty<Department> department;
	private StringProperty employeeName;
	private StringProperty employeeCode;
	private StringProperty contarctType;
	private StringProperty jobTitle;
	private StringProperty birthCity;
	private ObjectProperty<LocalDate> birthDate;
	private ObjectProperty<Long> employeeId;
	private StringProperty directManagerName;
	private BooleanProperty status;
	public final StringProperty employeeNameProperty()

	{
		if (employeeName == null) {
			employeeName = new SimpleStringProperty("");

		}
		return employeeName;
	}
	public final StringProperty directManagerNameProperty()

	{
		if (directManagerName == null) {
			directManagerName = new SimpleStringProperty("");

		}
		return directManagerName;
	}
	public final StringProperty employeeCodeProperty()

	{
		if (employeeCode == null) {
			employeeCode = new SimpleStringProperty("");

		}
		return employeeCode;
	}
	public final StringProperty contarctTypeProperty()

	{
		if (contarctType == null) {
			contarctType = new SimpleStringProperty("");

		}
		return contarctType;
	}
	public final StringProperty jobTitleProperty()

	{
		if (jobTitle == null) {
			jobTitle = new SimpleStringProperty("");

		}
		return jobTitle;
	}
	public final StringProperty birthCityProperty()

	{
		if (birthCity == null) {
			birthCity = new SimpleStringProperty("");

		}
		return birthCity;
	}
	public final ObjectProperty<Long> employeeIdProperty()

	{
		if (employeeId == null) {
			employeeId = new SimpleObjectProperty<>();

		}
		return employeeId;
	}
	public final ObjectProperty<LocalDate> birthDateProperty()

	{
		if (birthDate == null) {
			birthDate = new SimpleObjectProperty<>();

		}
		return birthDate;
	}

	public final ObjectProperty<Department> departmentProperty()
	{
		if (department == null) {
			department = new SimpleObjectProperty<>();

		}
		return department;
	}
	public final BooleanProperty statusProperty()

	{
		if (status == null) {
			status = new SimpleBooleanProperty();

		}
		return status;
	}
}
